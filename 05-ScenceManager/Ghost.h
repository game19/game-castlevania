#define GHOST_SPEED_X 0.09f
#define GHOST_GRAVITY 0.005f

#include "GameObject.h"

#define GHOST_BBOX_WIDTH 16
#define GHOST_BBOX_HEIGHT 15
#define GHOST_BBOX_HEIGHT_DIE 9

#define GHOST_STATE_WALKING 100
#define GHOST_STATE_DIE 200

#define GHOST_ANI_WALKING 0
#define GHOST_ANI_DIE 1

class Ghost : public CGameObject
{
	virtual void Update(DWORD dt, vector<LPGAMEOBJECT>* coObjects = NULL);
	virtual void GetBoundingBox(float& left, float& top, float& right, float& bottom);
	virtual void Render();
public:
	Ghost(float X, float Y, int Direction);
	virtual ~Ghost();
	Ghost();
	virtual void SetState(int state);
};

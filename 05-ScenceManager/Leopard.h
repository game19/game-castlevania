#pragma once

#include "GameObject.h"

#define LEOPARD_WALKING_SPEED 0.09f;

#define LEOPARD_BBOX_WIDTH 26
#define LEOPARD_BBOX_HEIGHT 15
#define LEOPARD_BBOX_HEIGHT_DIE 15

#define LEOPARD_STATE_SIT 100
#define LEOPARD_STATE_JUMP 200
#define LEOPARD_STATE_WALKING 300
#define LEOPARD_STATE_DIE 200

#define LEOPARD_ANI_SIT		0
#define LEOPARD_ANI_JUMP	1
#define LEOPARD_ANI_WALK	2
//#define LEOPARD_ANI_DIE 3

class CLeopard : public CGameObject
{
	virtual void GetBoundingBox(float &left, float &top, float &right, float &bottom);
	virtual void Update(DWORD dt, vector<LPGAMEOBJECT> *coObjects);
	virtual void Render();

public:
	CLeopard();
	virtual void SetState(int state);
};
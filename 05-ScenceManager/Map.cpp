#include "Map.h"

CMap* CMap::__instance = NULL;

CMap::CMap()
{

}

CMap* CMap::GetInstance()
{
	if (__instance == NULL) __instance = new CMap();
	return __instance;
}

void CMap::Render()
{
	LPDIRECT3DTEXTURE9 Texture = CTextures::GetInstance()->Get(idTexture);
	CGame::GetInstance()->Draw(x, y, Texture, left, top, right, bottom);
}

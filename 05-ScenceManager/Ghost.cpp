﻿#include "Ghost.h"

Ghost::Ghost()
{
	SetState(GHOST_STATE_WALKING);
}

void Ghost::GetBoundingBox(float& left, float& top, float& right, float& bottom)
{
	left = x;
	top = y;
	right = x + GHOST_BBOX_WIDTH;

	if (state == GHOST_STATE_DIE)
		bottom = y + GHOST_BBOX_HEIGHT_DIE;
	else
		bottom = y + GHOST_BBOX_HEIGHT;
}

void Ghost::Update(DWORD dt, vector<LPGAMEOBJECT>* coObjects)
{
	CGameObject::Update(dt, coObjects);

	//
	// TO-DO: make sure Goomba can interact with the world and to each of them too!
	// 

	x += dx;
	y += dy;

	if (vx < 0 && x < 0) {
		x = 0; vx = -vx;
	}

	if (vx > 0 && x > 290) {
		x = 290; vx = -vx;
	}
}

void Ghost::Render()
{
	int ani = GHOST_ANI_WALKING;
	if (state == GHOST_STATE_DIE) {
		ani = GHOST_ANI_DIE;
	}

	animation_set->at(ani)->Render(x, y);

	//RenderBoundingBox();
}

void Ghost::SetState(int state)
{
	CGameObject::SetState(state);
	switch (state)
	{
	case GHOST_STATE_DIE:
		y += GHOST_BBOX_HEIGHT - GHOST_BBOX_HEIGHT_DIE + 1;
		vx = 0;
		vy = 0;
		break;
	case GHOST_STATE_WALKING:
		vx = -GHOST_SPEED_X;
	}
}

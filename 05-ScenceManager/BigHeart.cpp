#include "BigHeart.h"
#include "Brick.h"
#include "Items.h"
#include "Simon.h"
#include "Utils.h"


CItemBigHeart::CItemBigHeart()
{
	this->visible = false;
	this->SetState(BIG_HEART_STATE_DESTROY);
}

void CItemBigHeart::Update(DWORD dt, vector<LPGAMEOBJECT>* coObjects)
{

	CGameObject::Update(dt);

	if (this->isVisible() == true && this->tStartVisible != 0) {
		vy += ITEM_GRAVITY * dt;// simple fall down

		if (GetTickCount() - this->tStartVisible > TIME_VISIBLE_ITEM_BIG_HEART)
		{
			this->SetVisible(false);
			this->SetState(BIG_HEART_STATE_DESTROY);
		}
	}

	vector<LPCOLLISIONEVENT> coEvents;
	vector<LPCOLLISIONEVENT> coEventsResult;

	coEvents.clear();
	CalcPotentialCollisions(coObjects, coEvents);

	if (coEvents.size() == 0)
	{
		y += dy;
		x += dx;
	}
	else
	{
		float min_tx, min_ty, nx = 0, ny;
		float rdx, rdy;
		FilterCollision(coEvents, coEventsResult, min_tx, min_ty, nx, ny, rdx, rdy);

		x += min_tx * dx;
		y += min_ty * dy;

		for (UINT i = 0; i < coEventsResult.size(); ++i)
		{
			LPCOLLISIONEVENT e = coEventsResult[i];

			if (dynamic_cast<CBrick*>(e->obj))
			{
				// Block brick
				if (e->ny != 0)
				{
					y += 0.4f * e->ny;
					vy = 0;
				}
			}

			/*if (dynamic_cast<Simon*>(e->obj))
			{
				this->SetVisible(false);
				
			}*/
		}
	}

	// clean up collision events
	for (UINT i = 0; i < coEvents.size(); i++) delete coEvents[i];
}

void CItemBigHeart::Render()
{
	//animation_set->at(0)->Render(x, y);
	if (this->visible == true && state != BIG_HEART_STATE_DESTROY) {
		animation_set->at(0)->Render(x, y);
		//DebugOut(L"[INFO] HEART_STATE EXITS \n");
	}
	RenderBoundingBox();

	/*
	else {
		DebugOut(L"[INFO] HEART_STATE NOT EXITS \n");
		if (this->visible == true) {
			DebugOut(L"[INFO] VISIABLE == TRUE \n", this->visible);
		}
		else if (this->state == BIG_HEART_STATE_DESTROY) {
			DebugOut(L"[INFO] HEART_STATE DESTROY \n", state);
		}
		else {
			DebugOut(L"[INFO] BOTH FALSE \n", state);
		}
		
		
	}
		*/
	
	
}

void CItemBigHeart::GetBoundingBox(float& left, float& top, float& right, float& bottom)
{
	if ( state != BIG_HEART_STATE_DESTROY) {
		left = x;
		top = y;
		right = x + ITEM_BIG_HEART_BBOX_WIDTH;
		bottom = y + ITEM_BIG_HEART_BBOX_HEIGHT;
	}
	
}


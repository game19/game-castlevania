﻿#pragma once
#include "Game.h"
#include "Textures.h"
#include "Scence.h"
#include "GameObject.h"
#include "Brick.h"
#include "Mario.h"
#include "Zombie.h"
#include "Leopard.h"
#include "Simon.h"
#include "Torch.h"
#include "Map.h"
#include "MBoard.h"
#include "Bat.h"


class CPlayScene: public CScene
{
private:
	/*Xử lí liên quan tạo Bat*/
	DWORD TimeCreateBat; // thời điểm đã tạo BAT
	DWORD TimeWaitCreateBat; // thời gian phải chờ để tạo bot
	bool isAllowCreateBat; // cho phép tạo Bat
	int CountEnemyBat;

protected: 
	Simon *player;					// A play scene has to have player, right? 
	Board *board;
	vector<LPGAMEOBJECT> objects;
	vector<LMap> tileMaps;

	int widthMap;
	int idTexture;
	int colTileImage;
	int rowTile = 40;

	void _ParseSection_TEXTURES(string line);
	void _ParseSection_SPRITES(string line);
	void _ParseSection_ANIMATIONS(string line);
	void _ParseSection_ANIMATION_SETS(string line);
	void _ParseSection_OBJECTS(string line);
	void _ParseSection_INFO_MAP(string line);
	void _ParseSection_TILE_MAP(string line);
	
public: 
	CPlayScene(int id, LPCWSTR filePath);
	
	virtual void Load();
	virtual void Update(DWORD dt);
	virtual void Render();
	virtual void Unload();

	Simon * GetPlayer() { return player; } 

	//friend class CPlayScenceKeyHandler;
};

class CPlayScenceKeyHandler : public CScenceKeyHandler
{
public: 
	virtual void KeyState(BYTE *states);
	virtual void OnKeyDown(int KeyCode);
	virtual void OnKeyUp(int KeyCode) ;
	CPlayScenceKeyHandler(CScene *s) :CScenceKeyHandler(s) {};
};


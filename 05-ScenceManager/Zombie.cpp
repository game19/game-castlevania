#include "Zombie.h"
#include "Game.h"
#include "Simon.h"

CZombie::CZombie()
{
	this->visible = true;
	SetState(ZOMBIE_STATE_WALKING);

}


void CZombie::Update(DWORD dt, vector<LPGAMEOBJECT> *coObjects)
{
	CGameObject::Update(dt, coObjects);

	//
	// TO-DO: make sure ZOMBIE can interact with the world and to each of them too!
	// 
	vy += SIMON_GRAVITY * dt;

	vector<LPCOLLISIONEVENT> coEvents;
	vector<LPCOLLISIONEVENT> coEventsResult;

	coEvents.clear();

	CalcPotentialCollisions(coObjects, coEvents);

	x += dx;
	if (coEvents.size() == 0)
	{
		y += dy;
	}
	else
	{
		float min_tx, min_ty, nx = 0, ny;
		float rdx = 0;
		float rdy = 0;

		// TODO: This is a very ugly designed function!!!!
		FilterCollision(coEvents, coEventsResult, min_tx, min_ty, nx, ny, rdx, rdy);

		//
		// Collision logic with other objects
		//
		for (UINT i = 0; i < coEventsResult.size(); i++)
		{
			LPCOLLISIONEVENT e = coEventsResult[i];


		}
	}
	// clean up collision events
	for (UINT i = 0; i < coEvents.size(); i++) delete coEvents[i];
}

void CZombie::Render()
{
	int ani = ZOMBIE_ANI_WALKING;
	if (state == ZOMBIE_STATE_DIE) {
		ani = ZOMBIE_ANI_DIE;
	}
	int alpha = 255;
	nx = -1;
	animation_set->at(ani)->Render(x,y,nx,alpha);

	RenderBoundingBox();
}

void CZombie::SetState(int state)
{
	CGameObject::SetState(state);
	switch (state)
	{
		case ZOMBIE_STATE_DIE:
			y += ZOMBIE_BBOX_HEIGHT - ZOMBIE_BBOX_HEIGHT_DIE + 1;
			vx = 0;
			vy = 0;
			break;
		case ZOMBIE_STATE_WALKING:
			vx = -ZOMBIE_WALKING_SPEED;
			break;
	}
}


void CZombie::GetBoundingBox(float& left, float& top, float& right, float& bottom)
{
	left = x;
	top = y;
	right = x + ZOMBIE_BBOX_WIDTH;

	if (state == ZOMBIE_STATE_DIE)
		bottom = y + ZOMBIE_BBOX_HEIGHT_DIE;
	else
		bottom = y + ZOMBIE_BBOX_HEIGHT;
}

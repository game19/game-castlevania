#include "Bat.h"
#include "Items.h"

CBat::CBat()
{
	SetState(BAT_STATE_WALKING);
}

CBat::CBat(float y)
{
	this->yBackup = y;
	this->visible = true;
	SetState(BAT_STATE_WALKING);
}

void CBat::GetBoundingBox(float& left, float& top, float& right, float& bottom)
{
	if (state != BAT_STATE_DIE) {
		left = x;
		top = y;
		right = x + BAT_BBOX_WIDTH;
		bottom = y + BAT_BBOX_HEIGHT;
	}
}

void CBat::Update(DWORD dt, vector<LPGAMEOBJECT>* coObjects)
{
	CGameObject::Update(dt, coObjects);

	if (this->visible) {
		if (y - yBackup >= DeltaY)
		{
			vy = -BAT_SPEED_Y;
		}
		else
			if (y - yBackup <= -DeltaY)
			{
				vy = BAT_SPEED_Y;
			}

		x += dx;
		y += dy;

		if (vx < 0 && x < 0) {
			x = 0; vx = -vx;
		}

		if (vx > 0 && x > 290) {
			x = 290; vx = -vx;
		}
	}

	if (this->state == BAT_STATE_DIE && animation_set->at(BAT_ANI_DIE)->IsOver(TIME_BAT_DESTROYED) && this->visible == true) {
		this->SetVisible(false);
		CItems::GetInstance()->CheckAndDrop(this);
		return;
	}
}

void CBat::Render()
{

	int ani = BAT_ANI_WALKING;
	if (this->state == BAT_STATE_DIE) {
		animation_set->at(BAT_ANI_DIE)->Render(x, y);
	}
	else {
		if (vx > 0) {
			animation_set->at(ani)->Render(x, y, 1, 255);
		}
		else {
			animation_set->at(ani)->Render(x, y);
		}

	}

	RenderBoundingBox();
}

void CBat::SetState(int state)
{
	CGameObject::SetState(state);
	switch (state)
	{
	case BAT_STATE_DIE:
		//y = -20;
		vx = 0;
		vy = 0;
		break;
	case BAT_STATE_WALKING:
		vy = BAT_SPEED_Y;
		vx = BAT_WALKING_SPEED;
	}
}

#include <algorithm>
#include <assert.h>
#include "Utils.h"

#include "Simon.h"
#include "Game.h"
#include "Torch.h"
#include "BigHeart.h"
#include "Items.h"
#include "WhipUpgradeItem.h"
#include "DaggerItem.h"
#include "Map.h"
#include "Bat.h"
#include "SubWeapons.h"
#include "Dagger.h"
#include "Portal.h"

Simon* Simon::__instance = NULL;
Simon* Simon::GetInstance()
{
	if (__instance == NULL)__instance = new Simon();
	return __instance;
}

Simon::Simon(float x, float y) : CGameObject()
{
	SetState(SIMON_STATE_IDLE);

	start_x = x;
	start_y = y;
	this->x = x;
	this->y = y;
	untouchable = 0;
	whip = new CWhip();
	//dagger = new CDagger();
}

void Simon::Update(DWORD dt, vector<LPGAMEOBJECT>* coObjects)
{
	// Calculate dx, dy 
	CGameObject::Update(dt);
	int widthMap = CMap::GetInstance()->getWidthMap();

	if (x <= -5) x = -5;
	if (x >= widthMap - 20) x = widthMap - 20;
	// Simple fall down
	vy += SIMON_GRAVITY * dt;
	

	vector<LPCOLLISIONEVENT> coEvents;
	vector<LPCOLLISIONEVENT> coEventsResult;

	coEvents.clear();

	if (state != SIMON_STATE_DIE)
		CalcPotentialCollisions(coObjects, coEvents);

	if (state == SIMON_STATE_LIFT_UP) { //lift simon up a little to avoid collision
		y -= SIMON_LIFT_UP;
		duck = 0; //set duck to false
	}

	for (UINT i = 0; i < coObjects->size(); i++)
	{
		LPGAMEOBJECT coliObject = coObjects->at(i);

		if (this->CheckCollision(coliObject)) {
			if (dynamic_cast<CItemBigHeart*>(coliObject)) {
				coliObject->SetVisible(false);
				coliObject->SetState(BIG_HEART_STATE_DESTROY);
			}
			else if (dynamic_cast<WhipUpgradeItem*>(coliObject)) {
				coliObject->SetVisible(true);
				this->SetState(SIMON_STATE_CHANGE_COLOR);
				coliObject->SetState(WHIP_UPGRADE_STATE_DESTROY);
				this->animation_set->at(SIMON_ANI_CHANGE_COLOR)->SetAniStartTime(GetTickCount());
				whip->UpItemWhip();
			}
			else if (dynamic_cast<DaggerItem*>(coliObject)) // if e->obj is platform
			{
				coliObject->SetState(DAGGER_ITEM_STATE_NOT_EXITS);
				coliObject->SetVisible(false);
				this->SetWeapon(SUBWEAPON_DAGGER);
				DebugOut(L"[INFO] Simon collided with Dagger Item:\n");
			}
		}
	}

	if(duck==1)
	{
		DebugOut(L"[INFO] Simon duck %d \n",y);
	}
		

	//ATTACK_UPDATE
	if (attack == 1 && useWeapon == 0) {
		if (jump == 0) {
			blockMovement = 1;
		}
		if (jump == 1) {
			blockMovement = 1;
			if (nx > 0) vx = SIMON_WALKING_SPEED;
			else vx = -SIMON_WALKING_SPEED;
		}

		if (GetTickCount64() - attack_start > animation_set->at(SIMON_ANI_WHIP)->GetTotalFrame())
		{
			attack_start = 0;
			blockMovement = 0;
			attack = 0;
			if (duck == 1 && releaseKey == 1) {
				releaseKey = 0;
				y -= SIMON_LIFT_UP;
				duck = 0;
			}
			ResetFrames(SIMON_ANI_WHIP);
			ResetFrames(SIMON_ANI_DUCK_ATTACK);
		}
		whip->SetOrientation(nx);
		whip->SetPositionWhip(D3DXVECTOR2(x, y), state == SIMON_STATE_WHIP ? true : false, duck);
		if (animation_set->at(SIMON_ANI_WHIP)->GetCurrentFrame() == 2 ||
			animation_set->at(SIMON_ANI_DUCK_ATTACK)->GetCurrentFrame() == 2)
			whip->Update(dt, coObjects);
	}
	else if  (attack==1 && useWeapon == 1) {
		DebugOut(L"hello attack \n");
		blockMovement = 1;
		if (GetTickCount64() - attack_start > SIMON_ATTACK_TIME)
		{
			attack_start = 0;
			attack = 0;
			blockMovement = 0;
			useWeapon = 0;
			if (duck == 1 && releaseKey == 1) {
				releaseKey = 0;
				y -= 9;
				duck = 0;
			}
			ResetFrames(SIMON_ANI_WHIP);
		}	
		if (animation_set->at(SIMON_ANI_WHIP)->GetCurrentFrame() == 2)
		{
			CSubWeapons::GetInstance()->UseSubWeapon(41);
		}
	}


	if (deflect == 1) {
		blockMovement = 1;
	}



	if (coEvents.size() == 0)
	{
		x += dx;
		y += dy;
	}
	else
	{
		float min_tx, min_ty, nx = 0, ny;
		float rdx = 0;
		float rdy = 0;

		// TODO: This is a very ugly designed function!!!!
		FilterCollision(coEvents, coEventsResult, min_tx, min_ty, nx, ny, rdx, rdy);

		// how to push back Mario if collides with a moving objects, what if Mario is pushed this way into another object?
		//if (rdx != 0 && rdx!=dx)
		//	x += nx*abs(rdx); 

		// block every object first!
		x += min_tx * dx + nx * 0.4f;
		y += min_ty * dy + ny * 0.4f;

		if (nx != 0) vx = 0;
		if (ny != 0) vy = 0;
		//
		// Collision logic with other objects
		//
		for (UINT i = 0; i < coEventsResult.size(); i++)
		{
			LPCOLLISIONEVENT e = coEventsResult[i];
			LPGAMEOBJECT coliObject = coObjects->at(i);
			if (dynamic_cast<CBrick*>(e->obj)) // if e->obj is platform
			{
				CBrick* platform = dynamic_cast<CBrick*>(e->obj);

				// reset jump when simon touch the ground
				if (e->ny < 0)
				{
					if (jump == 1) {
						jump = 0;
						blockLeft = 0;
						blockRight = 0;
					}

					//check if deflect state and collide platform
					if (deflect == 1 && x != collidePositionX) {
						deflect = 0;
						blockMovement = 0;
					}
				}
			}

			else if (dynamic_cast<CTorch*>(e->obj)) // if e->obj is platform
			{
				if (e->nx != 0) x += dx;
				if (e->ny != 0) y += dy;
			}
			//if (dynamic_cast<CBat*>(e->obj)) // if e->obj is platform
			//{
			//	

			//	e->obj->SetState(BAT_STATE_DIE);
			//	DebugOut(L"[INFO] Simon collided with bat: \n");

			//}
			else if (dynamic_cast<CItemBigHeart*>(e->obj)) // if e->obj is platform
			{
				if (e->nx != 0) x += dx;
				if (e->ny != 0) y += dy;

				e->obj->SetVisible(false);
				e->obj->SetState(BIG_HEART_STATE_DESTROY);
				DebugOut(L"[INFO] Simon collided with Big Heart:\n");

			}
			else if (dynamic_cast<WhipUpgradeItem*>(e->obj)) // if e->obj is platform
			{
				if (e->nx != 0) x += dx;
				if (e->ny != 0) y += dy;
				this->SetState(SIMON_STATE_CHANGE_COLOR);
				e->obj->SetState(WHIP_UPGRADE_STATE_DESTROY);
				e->obj->SetVisible(true);
				whip->UpItemWhip();
				DebugOut(L"[INFO] Simon collided with Whip Upgrade:\n");
			}
			else if (dynamic_cast<DaggerItem*>(e->obj)) // if e->obj is platform
			{
				if (e->nx != 0) x += dx;
				if (e->ny != 0) y += dy;
				this->SetWeapon(SUBWEAPON_DAGGER);
				e->obj->SetState(DAGGER_ITEM_STATE_NOT_EXITS);
				e->obj->SetVisible(false);

				DebugOut(L"[INFO] Simon collided with Dagger Item:\n");
			}


			if (untouchable == 0) {
				if (dynamic_cast<CZombie*>(e->obj)) // if e->obj is zombie
				{
					//TODO: check simon collide direction with enemies
					untouchable = 1;
					collidePositionX = x;
					collidePositionY = y;
					StartDeflect();
				}
			}

			if (this->CheckCollision(coliObject)) {
				if (dynamic_cast<CZombie*>(coliObject)) {
					//coObjects->at(i)->SetState(TORCH_STATE_NOT_EXSIST);
					//coObjects->at(i)->animation_set->at(TORCH_ANI_DESTROY)->SetAniStartTime(GetTickCount());
					/*coObjects->at(i)->animation_set->at(TORCH_ANI_DESTROYED)->SetAniStartTime(GetTickCount());*/
					DebugOut(L"Success");
				}
			}

			
		}

		// clean up collision events
		for (UINT i = 0; i < coEvents.size(); i++) delete coEvents[i];

	}
	
	
		
	
	

}

void Simon::ResetFrames(int ani) {
	animation_set->at(ani)->ResetFrame();
}

void Simon::Render()
{
	int ani = -1; //ani start with empty frame
	if (state == SIMON_STATE_DIE) {
		//ani = SIMON_ANI_DIE;
	}

	else if (deflect == 1) {
		ani = SIMON_ANI_DEFLECT;
	}

	else if (state == SIMON_STATE_CHANGE_COLOR) {
		ani = SIMON_ANI_CHANGE_COLOR;
	}
	else {
		if (state == SIMON_STATE_JUMP) {
			ani = SIMON_ANI_JUMP;
		}
		
		else if (state == SIMON_STATE_WHIP) {
			//blockMovement = 1;
			if (duck != 1) {
				ani = SIMON_ANI_WHIP;
			}
			//DUCK_ATTACK ANIMATION
			else {
				ani = SIMON_ANI_DUCK_ATTACK;
			}
		}

		else if (state == SIMON_STATE_DEFLECT) {
			ani = SIMON_ANI_IDLE;
		}

		else {
			if (vx == 0)
			{
				ani = SIMON_ANI_IDLE;
				ResetFrames(SIMON_ANI_WALKING); //fix simon glitch when walking
				if (duck == 1) {
					ani = SIMON_ANI_DUCK;
				}
			}
			else
			{
				ani = SIMON_ANI_WALKING;
			}
		}
	}

	int alpha = 255;
	//if (untouchable) alpha = 128;
	if (attack == 1 && duck == 0 && useWeapon == 0) { //attack at stand position
		animation_set->at(SIMON_ANI_WHIP)->Render(x, y, nx, alpha);
		whip->RenderbyFrame(animation_set->at(SIMON_ANI_WHIP)->GetCurrentFrame());
	}
	else if (attack == 1 && duck == 1 && useWeapon == 0) { //attack at duck position
		animation_set->at(SIMON_ANI_DUCK_ATTACK)->Render(x, y, nx, alpha);
		whip->RenderbyFrame(animation_set->at(SIMON_ANI_DUCK_ATTACK)->GetCurrentFrame());
	}
	else if (jump == 1) {
		animation_set->at(SIMON_ANI_JUMP)->Render(x, y, nx, alpha);
	}
	else if (state == SIMON_STATE_CHANGE_COLOR) {
		animation_set->at(SIMON_ANI_CHANGE_COLOR)->Render(x, y, nx, alpha);
	}
	else if (attack == 1 && useWeapon == 1) {
		animation_set->at(SIMON_ANI_WHIP)->Render(x, y, nx, alpha);
	}
	else 
		animation_set->at(ani)->Render(x, y, nx, alpha);
	//RenderBoundingBox();

		
}

void Simon::SetState(int state)
{
	CGameObject::SetState(state);

	switch (state)
	{
	case SIMON_STATE_WALKING_RIGHT:
		if (blockRight != 1) {
			if (blockMovement != 1) {
				vx = SIMON_WALKING_SPEED;
				nx = 1;
			}

			if (attack != 1 && duck == 1) {
				nx = 1;
			}

			if (jump == 1) blockLeft = 1;
		}
		break;
	case SIMON_STATE_CHANGE_COLOR:
		DebugOut(L"State change Color");
		vx = 0;
		animation_set->at(SIMON_ANI_CHANGE_COLOR)->Reset();
		animation_set->at(SIMON_ANI_CHANGE_COLOR)->SetAniStartTime(GetTickCount64());
		break;
	case SIMON_STATE_WALKING_LEFT:
		if (blockLeft != 1) {
			if (blockMovement != 1) {
				vx = -SIMON_WALKING_SPEED;
				nx = -1;
			}

			if (attack != 1 && duck == 1) {
				nx = -1;
			}

			if (jump == 1) blockRight = 1;
		}
		break;
	case SIMON_STATE_JUMP:
		vy = -SIMON_JUMP_SPEED_Y;
		break;
	case SIMON_STATE_IDLE:
		if(deflect == 0)
			vx = 0;
		break;

	case SIMON_STATE_DUCK:
		vx = 0;
		duck = 1;
		blockMovement = 1;
		break;

	case SIMON_STATE_LIFT_UP:
		vx = 0; //lift simon up to avoid collision with the ground => basically simon don't need to move during this
		break;

	case SIMON_STATE_WHIP:
		if (blockMovement != 1 || state != SIMON_STATE_JUMP) {//block movement when attack but when jump still allow movement
			vx = 0; //stop simon when whip is true
			/*whip->SetState(WHIP_STATE_ATTACK);*/
		}
		break;

	case SIMON_STATE_DEFLECT:
		vy -= SIMON_JUMP_DEFLECT_SPEED;
		vx -= 0.06f;
		blockMovement = 1;
		break;

	case SIMON_STATE_DIE:
		//vy = -SIMON_DIE_DEFLECT_SPEED;
		break;
	}
}

void Simon::GetBoundingBox(float& left, float& top, float& right, float& bottom)
{
	left = x;
	top = y;

	if (nx > 0) {
		left = x + 7;
	}
	if (nx < 0) {
		left = x + 5;
	}

	if ((nx < 0) && vx != 0) {
		left = x + 7;
	}

	//when ducking reduce bounding box of simon by SIMON_BBOX_DUCK_HEIGHT
	if (duck == 1) {

		right = left + SIMON_BBOX_WIDTH;
		bottom = top + 29;
		top = y + 8;
	}
	else {
		right = left + SIMON_BBOX_WIDTH;
		bottom = top + SIMON_BBOX_HEIGHT;
	}

}

void Simon::Reset()
{
	SetState(SIMON_STATE_IDLE);
	SetPosition(start_x, start_y);
	SetSpeed(0, 0);
}
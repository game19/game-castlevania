#pragma once
#include  "GameObject.h"
#include "Whip.h"
#include "Brick.h"
#include "Dagger.h"
#include "Torch.h"
#include "Zombie.h"

#define SIMON_WALKING_SPEED			0.05f
#define SIMON_JUMP_SPEED_Y			0.25f
#define SIMON_JUMP_DEFLECT_SPEED	0.24f
#define SIMON_GRAVITY				0.0006f

#define SIMON_STATE_IDLE			0
#define SIMON_STATE_WALKING_RIGHT	100
#define SIMON_STATE_WALKING_LEFT	200
#define SIMON_STATE_JUMP			300
#define SIMON_STATE_WHIP			400
#define SIMON_STATE_DUCK			500
#define SIMON_STATE_ATTACK_SUBWEAPON	501
#define SIMON_STATE_LIFT_UP			600	//lift simon up a little after duck to avoid out of map
#define SIMON_STATE_DEFLECT			700
#define SIMON_STATE_CHANGE_COLOR	799
#define SIMON_STATE_STAIR			800
#define SIMON_STATE_UNTOUCHABLE		998
#define SIMON_STATE_DIE				999

#define SIMON_TIME_CHANGE_COLOR	1000

#define SIMON_ANI_IDLE				0
#define SIMON_ANI_WALKING			1
#define SIMON_ANI_WHIP				2
#define SIMON_ANI_DUCK				3
#define SIMON_ANI_JUMP				3
#define SIMON_ANI_DUCK_ATTACK		4
#define SIMON_ANI_CHANGE_COLOR		5
#define SIMON_ANI_DEFLECT			6
#define SIMON_ANI_STAIR_UP			7
#define SIMON_ANI_STAIR_DOWN		8

#define SIMON_ATTACK_TIME			450
#define SIMON_DEFLECT_TIME			500
#define SIMON_UNTOUCHABLE_TIME		1000

//SIMON BOUNDING BOX
#define SIMON_BBOX_WIDTH			18
#define SIMON_BBOX_HEIGHT			30
#define SIMON_BBOX_DUCK_HEIGHT		24

#define SIMON_LIFT_UP				2
#define SIMON_MAX_DEFLECT_HEIGHT	10

#define SUBWEAPON_DAGGER 2


class Simon : public CGameObject 
{
	static Simon* __instance;
	int attack_time;
	DWORD attack_start;
	DWORD deflect_start;
	DWORD untouchable_start;
	float start_x;
	float start_y;
	CWhip* whip;
	/*CDagger* dagger;*/
	int currentWeapon = 0;
	float collidePositionX = 0.0f; //used for simon position when collide happned X
	float collidePositionY = 0.0f; //used for simon position when collide happned X
public:
#pragma region methods
	void SetWeapon(int CurrentWeapon) { this->currentWeapon = CurrentWeapon;  }
	int GetWeapon() { return currentWeapon; }

	Simon(float x = 0.0f, float y = 0.0f);
	static Simon* GetInstance();
	virtual void Update(DWORD dt, vector<LPGAMEOBJECT>* colliable_objects = NULL);
	virtual void Render();

	void SetState(int state);

	void Reset();
	void StartUseWeapon(){ attack_start = GetTickCount(); useWeapon= 1 ; SetState(SIMON_STATE_WHIP); }
	void StartJump() { jump = 1; SetState(SIMON_STATE_JUMP); }
	void StartAttack() { attack_start = GetTickCount64();attack =1 ; SetState(SIMON_STATE_WHIP); }
	void StartDeflect() {deflect = 1; SetState(SIMON_STATE_DEFLECT); }
	void StartUntouchable() { untouchable_start = GetTickCount64(); untouchable = 1; SetState(SIMON_STATE_UNTOUCHABLE); }
	virtual void GetBoundingBox(float& left, float& top, float& right, float& bottom);
#pragma endregion

#pragma region variables
	int attack = 0;		//simon is attack or not
	int duck = 0;
	int jump = 0;
	int deflect = 0;
	int untouchable = 0;
	int useWeapon = 0;
	int CombineStates(int state_1, int state_2);
	void ResetFrames(int ani); //Reset frame when finished animations
	int blockMovement = 0;
	int blockLeft = 0;
	int blockRight = 0;
	int releaseKey = 0; //check release key state
	int isOnStair = 0;
#pragma endregion 
};


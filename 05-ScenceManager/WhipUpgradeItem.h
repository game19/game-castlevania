#pragma once


#include "GameObject.h"

#define ITEM_CHAIN_BBOX_WIDTH	16
#define ITEM_CHAIN_BBOX_HEIGHT	16

#define TIME_VISIBLE_ITEM_CHAIN 20000
#define WHIP_UPGRADE_STATE_EXITS 0
#define WHIP_UPGRADE_STATE_DESTROY 1


class WhipUpgradeItem :public CGameObject
{
public:
	WhipUpgradeItem();
	void Update(DWORD dt, vector<LPGAMEOBJECT>* coObjects);
	void Render();
	void GetBoundingBox(float& left, float& top, float& right, float& bottom);
};
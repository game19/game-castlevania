#pragma once
#include "GameObject.h"
#include "Utils.h"

#define BAT_SPEED_X 0.1f 
#define BAT_SPEED_Y 0.05f

#define BAT_WALKING_SPEED 0.05f;

#define BAT_BBOX_WIDTH 16
#define BAT_BBOX_HEIGHT 15
#define BAT_BBOX_HEIGHT_DIE 9

#define BAT_STATE_WALKING 100
#define BAT_STATE_DIE 200

#define BAT_ANI_WALKING 0
#define BAT_ANI_DIE 2
#define TIME_BAT_DESTROYED 360

#define DeltaY 20

class CBat : public CGameObject
{
	virtual void GetBoundingBox(float& left, float& top, float& right, float& bottom);
	virtual void Update(DWORD dt, vector<LPGAMEOBJECT>* coObjects);
	virtual void Render();
private:
	float yBackup;
public:
	CBat();
	CBat(float y);
	virtual void SetState(int state);
};
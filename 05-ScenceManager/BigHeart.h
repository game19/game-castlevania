
#pragma once
#include "GameObject.h"

#define ITEM_BIG_HEART_BBOX_WIDTH		12
#define ITEM_BIG_HEART_BBOX_HEIGHT	10

#define TIME_VISIBLE_ITEM_BIG_HEART 5000
#define BIG_HEART_STATE_DESTROY 1
#define BIG_HEART_STATE_EXITS 0

class CItemBigHeart :public CGameObject
{
public:
	CItemBigHeart();
	void Update(DWORD dt, vector<LPGAMEOBJECT>* coObjects);
	void Render();
	void GetBoundingBox(float& left, float& top, float& right, float& bottom);
};
#include "Whip.h"
#include "Simon.h"
#include "Torch.h"
#include "Utils.h"
#include "Bat.h"


CWhip::CWhip():CGameObject()
{
	SetState(NORMAL_WHIP);
}

void CWhip::Update(DWORD dt, vector<LPGAMEOBJECT>* coObjects)
{
	
	for (UINT i = 0; i < coObjects->size(); i++)
	{
		LPGAMEOBJECT coliObject = coObjects->at(i);

		if (this->CheckCollision(coliObject)) {
			if (dynamic_cast<CTorch*>(coliObject)) {
				coObjects->at(i)->SetState(TORCH_STATE_NOT_EXSIST);
				coObjects->at(i)->animation_set->at(TORCH_ANI_DESTROY)->SetAniStartTime(GetTickCount());
			}
			if (dynamic_cast<CBat*>(coliObject)) {
				coObjects->at(i)->SetState(BAT_STATE_DIE);
				coObjects->at(i)->animation_set->at(BAT_ANI_DIE)->SetAniStartTime(GetTickCount());
			}
		}
	}
}

void CWhip::Render() {
	/*if (state == WHIP_STATE_ATTACK) {
		animation_set->at(WHIP_NORMAL_ANI)->Render(x, y, nx, 255);
	}*/
}

void CWhip::RenderbyFrame(int currentFrame)
{
	int StateWhip = CWhip::GetInstance()->GetState();
	CAnimationSets::GetInstance()->Get(WHIP_NORMAL_ANI)->at(StateWhip)->RenderByFrame(currentFrame, nx, x, y, 255);
	if (currentFrame == 2) {
		RenderBoundingBox();
	}
}

void CWhip::SetState(int state)
{
	this->state = state;
}

void CWhip::GetBoundingBox(float& left, float& top, float& right, float& bottom) {
	switch (CWhip::GetInstance()->GetState()) {
	case NORMAL_WHIP:{
		top = y + 4;
		bottom = top + 8;
		if (nx > 0) left = x + 70;
		else
		{
			left = x + 26;
		}
		right = left + 24;
		break; 
	}
	case SHORT_CHAIN: {
		top = y + 4;
		bottom = top + 8;
		if (nx > 0) left = x + 70;
		else
		{
			left = x + 26;
		}
		right = left + 24;
		break;
	}
	case LONG_CHAIN: {
		top = y + 4;
		bottom = top + 8;
		if (nx > 0) left = x + 70;
		else
		{
			left = x + 12;
		}
		right = left + 40;
		break;
	}
	}
}


void CWhip::SetPositionWhip(D3DXVECTOR2 simonPosition, bool isAttacking, int duck)
{
	if (duck == 1) {
		if (nx > 0)
		{
			simonPosition.x -= 41.0f;
			if (isAttacking) simonPosition.y -= 1.0f;
			else simonPosition.y += 10.0f;
		}
		else
		{
			simonPosition.x -= 49.0f;
			if (isAttacking) simonPosition.y += 6.0f;
			else simonPosition.y += 10.0f;
		}
	}

	else {
		if (nx > 0)
		{
			simonPosition.x -= 41.0f;
			if (isAttacking) simonPosition.y -= 1.0f;
			else simonPosition.y += 4.0f;
		}
		else
		{
			simonPosition.x -= 49.0f;
			if (isAttacking) simonPosition.y += 6.0f;
			else simonPosition.y += 4.0f;
		}
	}
	

	SetPosition(simonPosition.x, simonPosition.y);
}

void CWhip::UpItemWhip()
{
	switch (CWhip::GetInstance()->GetState())
	{
	case NORMAL_WHIP:
		CWhip::GetInstance()->SetState(SHORT_CHAIN);
		break;
	case SHORT_CHAIN:
		CWhip::GetInstance()->SetState(LONG_CHAIN);
		break;
	default:
		CWhip::GetInstance()->SetState(NORMAL_WHIP);
		break;
	}
}


CWhip* CWhip::__instance = NULL;
CWhip* CWhip::GetInstance()
{
	if (__instance == NULL) __instance = new CWhip();
	return __instance;
}

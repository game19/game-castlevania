#include "Leopard.h"

CLeopard::CLeopard()
{
	this->visible = true;
	SetState(LEOPARD_STATE_SIT);
}

void CLeopard::GetBoundingBox(float &left, float &top, float &right, float &bottom)
{
	left = x;
	top = y;
	right = x + LEOPARD_BBOX_WIDTH;

	if (state == LEOPARD_STATE_DIE)
		bottom = y + LEOPARD_BBOX_HEIGHT_DIE;
	else
		bottom = y + LEOPARD_BBOX_HEIGHT;
}

void CLeopard::Update(DWORD dt, vector<LPGAMEOBJECT> *coObjects)
{
	CGameObject::Update(dt, coObjects);

	//
	// TO-DO: make sure LEOPARD can interact with the world and to each of them too!
	// 

	x += dx;
	y += dy;

	/*if (vx < 0 && x < 0) {
		x = 0; vx = -vx;
	}

	if (vx > 0 && x > 290) {
		x = 290; vx = -vx;
	}*/
}

void CLeopard::Render()
{
	int ani = LEOPARD_ANI_SIT;
	if (state == LEOPARD_STATE_SIT) {
		ani = LEOPARD_ANI_SIT;
	}
	else if (state == LEOPARD_STATE_WALKING) {
		ani = LEOPARD_ANI_WALK;
	}

	animation_set->at(ani)->Render(x, y);

	RenderBoundingBox();
}

void CLeopard::SetState(int state)
{
	CGameObject::SetState(state);
	switch (state)
	{
	case LEOPARD_STATE_DIE:
		y += LEOPARD_BBOX_HEIGHT - LEOPARD_BBOX_HEIGHT_DIE + 1;
		vx = 0;
		vy = 0;
		break;
	case LEOPARD_STATE_WALKING:
		vx = -LEOPARD_WALKING_SPEED;
	case LEOPARD_STATE_SIT:
		vx = 0;
	}

}
#pragma once
#include "GameObject.h"


#define TORCH_STATE_NOT_EXSIST 1
#define TORCH_STATE_EXSIST 0
#define TORCH_STATE_ITEM 2
#define TORCH_STATE_ITEM_NOT_EXSIST 3
#define TORCH_ANI_DESTROY 1



#define TIME_TORCH_DESTROYED 360

#define HEIGHT_TORCH	32
#define WIDTH_TORCH	16

class CTorch : public CGameObject {
	static CTorch* __instance;

	virtual void Render();
	virtual void Update(DWORD dt, vector<LPGAMEOBJECT>* coObjects = NULL);
	virtual void GetBoundingBox(float& left, float& top, float& right, float& bottom);
	virtual void SetPosition(float _x, float _y) { x = _x; y = _y; }
	virtual void SetState(int state) { this->state = state; }

public:
	CTorch();
	static CTorch* GetInstance();
};
#pragma once
#include "GameObject.h"

#define ITEM_DAGGER_BBOX_WIDTH	16
#define ITEM_DAGGER_BBOX_HEIGHT	9

#define TIME_VISIBLE_ITEM_DAGGER 2000

#define DAGGER_ITEM_STATE_EXITS 0
#define DAGGER_ITEM_STATE_NOT_EXITS 1


class DaggerItem :public CGameObject
{
public:
	DaggerItem();
	void Update(DWORD dt, vector<LPGAMEOBJECT>* coObjects);
	void Render();
	void GetBoundingBox(float& left, float& top, float& right, float& bottom);
};
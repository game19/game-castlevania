#pragma once
#include "GameObject.h"

#define WHIP_NORMAL_ANI	2

#define WHIP_STATE_NONE		0
#define WHIP_STATE_ATTACK	100

#define NORMAL_WHIP		0
#define SHORT_CHAIN		1
#define LONG_CHAIN		2



class CWhip : public CGameObject {
	static CWhip* __instance;
	
	virtual void GetBoundingBox(float& left, float& top, float& right, float& bottom);

public:
	CWhip();
	virtual void SetState(int state);
	virtual void Render();
	virtual void RenderbyFrame(int currentFrame);
	virtual void Update(DWORD dt, vector<LPGAMEOBJECT>* coObjects = NULL);
	void SetPositionWhip(D3DXVECTOR2 simonPosition, bool isStanding, int duck);
	void UpItemWhip();
	static CWhip* GetInstance();
};
#pragma once
#include <Windows.h>
#include <d3dx9.h>
#include <unordered_map>

#include "Sprites.h"

/*
Sprite animation
*/
class CAnimationFrame
{
	LPSPRITE sprite;
	DWORD time;

public:
	CAnimationFrame(LPSPRITE sprite, int time) { this->sprite = sprite; this->time = time; }
	DWORD GetTime() { return time; }
	LPSPRITE GetSprite() { return sprite; }
};

typedef CAnimationFrame *LPANIMATION_FRAME;

class CAnimation
{
	DWORD lastFrameTime;
	DWORD aniStartTime;
	int currentFrame;
	int defaultTime;
	vector<LPANIMATION_FRAME> frames;
public:
	CAnimation(int defaultTime = 100) { this->defaultTime = defaultTime; lastFrameTime = -1; currentFrame = -1; }
	void Add(int spriteId, DWORD time = 0);
	void ResetFrame() { currentFrame = -1; }
	void Render(float x, float y, int alpha = 255);
	void Render(float x, float y, int trend ,int alpha = 255);	//render only frame on left then mirror
	void RenderByFrame(int frameID, int nx, float x, float y,int alpha); //Render with frame custom
	int GetCurrentFrame() {
		return currentFrame;
	}
	bool IsOver(DWORD dt) { return GetTickCount() - aniStartTime >= dt; };
	void Reset() { currentFrame = -1; }
	void SetAniStartTime(DWORD t) { aniStartTime = t; }
	int GetLastRenderFrame() {
		return frames.size() - 1;
	}
	DWORD GetTotalFrame() {
		return frames[0]->GetTime() * frames.size();
	}
};

typedef CAnimation *LPANIMATION;

class CAnimations
{
	static CAnimations * __instance;

	unordered_map<int, LPANIMATION> animations;

public:
	void Add(int id, LPANIMATION ani);
	LPANIMATION Get(int id);
	void Clear();

	static CAnimations * GetInstance();
};

typedef vector<LPANIMATION> CAnimationSet;

typedef CAnimationSet* LPANIMATION_SET;

/*
	Manage animation set database
*/
class CAnimationSets
{
	static CAnimationSets * __instance;

	unordered_map<int, LPANIMATION_SET> animation_sets;

public:
	CAnimationSets();
	void Add(int id, LPANIMATION_SET ani);
	LPANIMATION_SET Get(unsigned int id);


	static CAnimationSets * GetInstance();
};
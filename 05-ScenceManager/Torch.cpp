#include "Torch.h"
#include "Items.h"
#include "Whip.h"
#include "Utils.h"
#include "Simon.h"
#include "Brick.h"

CTorch::CTorch() :CGameObject()
{
	
	SetState(TORCH_STATE_EXSIST);
}

void CTorch::Render() {
	if (this->isVisible()) {
		if (!state == TORCH_STATE_NOT_EXSIST) {
			animation_set->at(0)->Render(x, y);
		}
		else {
			animation_set->at(1)->Render(x, y);
		}
	}

	RenderBoundingBox();
}


void CTorch::Update(DWORD dt, vector<LPGAMEOBJECT>* coObjects)
{
	if (this->state == TORCH_STATE_NOT_EXSIST && animation_set->at(TORCH_ANI_DESTROY)->IsOver(TIME_TORCH_DESTROYED) && this->visible == true) {
		this->SetVisible(false);
  		CItems::GetInstance()->CheckAndDrop(this);
		return;
	}
}

void CTorch::GetBoundingBox(float& left, float& top, float& right, float& bottom) {
	if (state == TORCH_STATE_EXSIST) {
		left = x;
		top = y;
		bottom = top + HEIGHT_TORCH;
		right = left + WIDTH_TORCH;
	}
	
}


CTorch* CTorch::__instance = NULL;
CTorch* CTorch::GetInstance()
{
	if (__instance == NULL) __instance = new CTorch();
	return __instance;
}
